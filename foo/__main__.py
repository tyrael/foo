#!/usr/bin/env python
# encoding: utf-8
import sys
from .classmodule import FooCommand


def main():
    argv = sys.argv
    cmd = FooCommand()

    cmd.execute_from_commandline(argv)

if __name__ == '__main__':
    main()
