#!/usr/bin/env python
# encoding: utf-8


from setuptools import setup
setup(
    name = 'foo',
    version = '0.1.0',
    packages = ['foo'],
    entry_points = {
        'console_scripts': [
            'foo = foo.__main__:main'
        ]
    })
